package ltd.pvt.technologies.vendor.doortask.doortaskvendor.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


import javax.inject.Inject;

import ltd.pvt.technologies.vendor.doortask.doortaskvendor.R;


public class NetworkConnection {

    Activity currentActivity;


    public NetworkConnection(Activity activity) {
        this.currentActivity = activity;

    }

    public void CheckConnection() {
        if (isOnline()) {
            Log.i("Networkstatus", "Online");
        } else {
            currentActivity.setContentView(R.layout.activity_no_internet);
            Log.i("Networkstatus", "No Network");
        }
    }


    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) currentActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}
