package ltd.pvt.technologies.vendor.doortask.doortaskvendor.view;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.concurrent.TimeUnit;

import ltd.pvt.technologies.vendor.doortask.doortaskvendor.R;
import ltd.pvt.technologies.vendor.doortask.doortaskvendor.model.UserInfo;

public class OtpVerficationActivity extends AppCompatActivity {
    private String mVerificationId;

    private FirebaseAuth mAuth;
    String ccp;
    String mobile;
    PinView pinView;
    Button login;

    EditText username, useremail;

    TextView otptickercounter;
    FirebaseFirestore userDb = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verfication);
        Intent intent = getIntent();
        mobile = intent.getStringExtra("mobile");
        ccp = intent.getStringExtra("ccp");

        Log.i("number", ccp + "" + mobile);
        pinView = findViewById(R.id.pinView);
        login = findViewById(R.id.loginbutton);
        otptickercounter = findViewById(R.id.otpticker);
        mAuth = FirebaseAuth.getInstance();


        username = findViewById(R.id.log_uname);
        useremail = findViewById(R.id.log_email);


        sendVerificationCode(mobile);
        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().trim().length() == 6) {
                    login.setVisibility(View.VISIBLE);
                } else {
                    login.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = pinView.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    pinView.setError("Enter valid code");
                    pinView.setFocusable(true);
                    return;
                }

                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });


        if (otptickercounter.getText() == "Resend") {
            otptickercounter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendVerificationCode(mobile);
                    startCounter();
                }
            });
        }
    }


    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+" + ccp + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                pinView.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(OtpVerficationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
            Log.i("otpsent", s);
        }
    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(OtpVerficationActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //verification successful we will start the profile activity


                            UserInfo userInfo = new UserInfo();
                            userInfo.setPhonenumber(mAuth.getCurrentUser().getPhoneNumber());
                            userInfo.setUid(mAuth.getCurrentUser().getUid());
                            userInfo.setAddress("Address");
                            userInfo.setEmail(useremail.getText().toString());
                            userInfo.setName(username.getText().toString());
                            userDb.collection("vendors").document(mAuth.getCurrentUser().getUid()).set(userInfo);

                            Intent intent = new Intent(OtpVerficationActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Toast.makeText(OtpVerficationActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCounter();

    }

    private void startCounter() {
        CountDownTimer myCountDown = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                //update the UI with the new count
                otptickercounter.setText("Enter OTP of 6 Digit sent on " + mobile + " in \n" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                //start the activity
                otptickercounter.setText("Resend");
            }
        };
//start the countDown
        myCountDown.start();
    }
}
