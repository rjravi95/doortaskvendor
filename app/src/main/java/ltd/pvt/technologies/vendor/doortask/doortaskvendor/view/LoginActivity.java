package ltd.pvt.technologies.vendor.doortask.doortaskvendor.view;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.hbb20.CountryCodePicker;

import ltd.pvt.technologies.vendor.doortask.doortaskvendor.R;

public class LoginActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText phonenumber;
    Button loginbutton;
    String mobile;
    CountryCodePicker countryCodePicker;
    int Permission_All = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        phonenumber = findViewById(R.id.phone_number);
        loginbutton = findViewById(R.id.continuebtn);
        countryCodePicker = findViewById(R.id.phone_num_ccp);

        mAuth = FirebaseAuth.getInstance();
        phonenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (s.toString().trim().length() == 10) {
                    loginbutton.setVisibility(View.VISIBLE);
                } else {
                    loginbutton.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                mobile = s.toString();
            }
        });

        loginbutton.setOnClickListener(new View.OnClickListener() {

            String ccp = countryCodePicker.getSelectedCountryCode();


            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, OtpVerficationActivity.class);
                intent.putExtra("mobile", mobile);
                intent.putExtra("ccp", ccp);
                startActivity(intent);

            }
        });


        String[] Permissions = {
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS
        };
        if (!hasPermissions(this, Permissions)) {
            ActivityCompat.requestPermissions(this, Permissions, Permission_All);
        }


        if (hasPermissions(this, Permissions)) {

            if (mAuth.getCurrentUser() != null) {
                finish();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }


        } else {
            if (!hasPermissions(this, Permissions)) {
                ActivityCompat.requestPermissions(this, Permissions, Permission_All);
            }
            Toast.makeText(this, "Check permissions", Toast.LENGTH_SHORT).show();
            if (mAuth.getCurrentUser() != null) {
                finish();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }

        }


    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.ic_launcher_round)
                .setTitle("Closing DoorTask")
                .setMessage("Are you sure you want to close DoorTask ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

}
