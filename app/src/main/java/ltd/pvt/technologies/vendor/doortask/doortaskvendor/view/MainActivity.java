package ltd.pvt.technologies.vendor.doortask.doortaskvendor.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import ltd.pvt.technologies.vendor.doortask.doortaskvendor.R;
import ltd.pvt.technologies.vendor.doortask.doortaskvendor.util.NetworkConnection;

public class MainActivity extends AppCompatActivity {

    private static final String SELECTED_ITEM = "arg_selected_item";
    public BottomNavigationView navigation;
    private int mSelectedItem;
    private MenuItem selectedItem;

    NetworkConnection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connection = new NetworkConnection(this);
        navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item);
                return true;
            }
        });

        if (savedInstanceState != null) {
            mSelectedItem = savedInstanceState.getInt(SELECTED_ITEM, 0);
            selectedItem = navigation.getMenu().findItem(mSelectedItem);
        } else {
            selectedItem = navigation.getMenu().getItem(0);
        }

        selectFragment(selectedItem);

    }



    public void selectFragment(MenuItem item) {
        Fragment frag = null;
        item.setCheckable(true);
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.nav_new_works:

                NewMessages fragmentone = new NewMessages();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.main_container, fragmentone);
                ft.commit();


                break;
            case R.id.nav_ongoing:


                OnGoingTask fragmentone_Two = new OnGoingTask();
                FragmentTransaction ft_two = getSupportFragmentManager().beginTransaction();
                ft_two.replace(R.id.main_container, fragmentone_Two);
                ft_two.commit();

                break;
            case R.id.nav_profile:

                ProfileFragment fragmentone_Three = new ProfileFragment();
                FragmentTransaction ft_three = getSupportFragmentManager().beginTransaction();
                ft_three.replace(R.id.main_container, fragmentone_Three);
                ft_three.commit();

                break;


        }

        // update selected item
        mSelectedItem = item.getItemId();


        if (frag != null) {
            NewMessages fragmentone = new NewMessages();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragmentone);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {


        MenuItem homeItem = navigation.getMenu().getItem(0);

        if (mSelectedItem != homeItem.getItemId()) {

            navigation.setSelectedItemId(homeItem.getItemId());


        } else {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.ic_launcher_round)
                    .setTitle("Closing Vendor Doortask")
                    .setMessage("Are you sure you want to close Doortask ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }


    }

    @Override
    protected void onResume() {
        connection.CheckConnection();
        super.onResume();

    }
}
