package ltd.pvt.technologies.vendor.doortask.doortaskvendor.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ltd.pvt.technologies.vendor.doortask.doortaskvendor.R;
import ltd.pvt.technologies.vendor.doortask.doortaskvendor.viewmodel.NewMessagesViewModel;

public class NewMessages extends Fragment {

    private NewMessagesViewModel mViewModel;

    public static NewMessages newInstance() {
        return new NewMessages();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_messages_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(NewMessagesViewModel.class);
        // TODO: Use the ViewModel
    }

}
